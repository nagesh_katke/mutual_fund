import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { PlatformLocation } from '@angular/common';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-orderhistory',
  templateUrl: './orderhistory.component.html',
  styleUrls: ['./orderhistory.component.css']
})

export class OrderhistoryComponent implements OnInit {
  viewMode = 'tab1';
  order: any;
  sipOrder: any;
  loading: boolean;
  errorMessage: any;
  errorStatus: any;
  otherError: boolean;
  orderHistory = [];
  sipHistory = [];
  noInvest = false;
  noInvestSip = false;
  SipDetails: string;
  previousUrl: any;
  prev_url: string;

  constructor(private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) { 
    this.getorderHistory();
    this.sipDate();
    this.loading = true;

    // location.onPopState((event) => {
    //   console.log('hello');
    //   if (this.SipDetails === 'Success') {
    //     this.router.navigate(['/home']);
    //     console.log('aaaaaaaaaaaaa');
        
    //   }
    // });
  }

  ngOnInit() {
    this.loading = true;
  }

  getorderHistory() {
    this.apiService.getOrderHistory().subscribe(
      res => {
        this.loading = false;
        this.orderHistory = res.data;
        // Condition to show no-investment and filter section
        if (this.orderHistory.length != 0) {
          this.noInvest = false;
        }
        else {
          this.noInvest = true;
        }
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
        if (this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
      }
    )
  }
  
  sipDate(){
    this.apiService.getSip().subscribe(
      res => {
        this.loading = false;
        this.sipHistory = res.data;
        // Condition to show no-investment and filter section
        if (this.sipHistory.length != 0) {
          this.noInvestSip = false;
        }
        else {
          this.noInvestSip = true;
        }
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
        if (this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
      }
    )
  }

  viewOrderDetails(order): void {
    var object = {};
    object['orderid'] = order.order_id;
    this.loading = true;

    this.apiService.getOrderDetails(object).subscribe(
      res => {
        let key = 'orderDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        this.router.navigateByUrl('/order-details');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
        if (this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
      }
    )
  }

  viewSipDetails(sipOrder): void {
    var object = {};
    object['isin'] = sipOrder.isin;
    object['sip_registration_no'] = sipOrder.sip_registration_no;
    this.loading = true;

    this.apiService.getSipTransaction(object).subscribe(
      res => {
        let key = 'SipDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        this.router.navigateByUrl('/order-history-sip');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
        if (this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
      }
    )
  }

}
