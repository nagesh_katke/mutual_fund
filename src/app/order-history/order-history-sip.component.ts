import { Component, OnInit, Inject } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-order-history-sip',
  templateUrl: './order-history-sip.component.html',
  styleUrls: ['./order-history-sip.component.css']
})

export class OrderHistorySipComponent implements OnInit {
  sipOrder: any;
  sipDetails: any;
  loading: boolean;
  errorMessage: any;
  otherError: boolean;
  errorStatus: any;
  modalReference: NgbModalRef;
  prev_ur: string;

  constructor(public router: Router, private apiService: ApiServiceService, private modalService: NgbModal, private errorHaldlingService: ErrorHaldlingService, private routerService: RouterExtService, public location: PlatformLocation) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
    let key = 'SipDetails';
    this.sipOrder = JSON.parse(localStorage.getItem(key));
    // console.log(typeof(this.sipOrder));
    

    const prev_url = this.routerService.getPreviousUrl();
    console.log(prev_url);
    
    if(this.sipOrder === 'Success'  ){
      this.router.navigate(['/home']);
  }
    
  }
  
  
  ngOnInit() {
  }

  // Function to open SIP modal
  openSipHistory(sipHistory) {
    this.modalReference = this.modalService.open(sipHistory, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }

  /*cancel sip*/
  CancelSip(): void {
    var object = {};
    object['isin'] = this.sipOrder.sip_details.isin;
    object['sip_registration_no'] = this.sipOrder.sip_details.sip_registration_no;
    object['scheme_code'] = this.sipOrder.sip_details.scheme_code;
    object['scheme_name'] = this.sipOrder.sip_details.scheme_name;
    object['order_type'] = 'cancel';
    this.loading = true;

    this.apiService.cancelSIP(object).subscribe(
      res => {
        let key = 'SipDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        this.router.navigateByUrl('/order-history');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
        if(this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
      }
    )
  }

  /*cancel sip*/
}
