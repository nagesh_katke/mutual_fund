import { Component, OnInit , Inject} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-edit-sip',
  templateUrl: './edit-sip.component.html',
  styleUrls: ['./edit-sip.component.css']
})

export class EditSipComponent implements OnInit {
  sipamount: any;
  submitted = false;
  sipAmtForm: FormGroup;
  sipamt:any;
  sipdate:any = '28th';

  constructor(private fb: FormBuilder, private router: Router, @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
    this.document.body.classList.add('order-history-background');

    this.sipAmtForm = this.fb.group({
      investSipAmt: ['', [Validators.required, Validators.min(500)]],
      sipdate: ['', [Validators.required]],
    });
  }

// convenience getter for easy access to form fields
get f() { return this.sipAmtForm.controls; }

editSip(): void {
  this.submitted = true;
  }

}
