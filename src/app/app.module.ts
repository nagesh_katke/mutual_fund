import { MbscModule } from '@mobiscroll/angular';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Import Font Awesome
import { AngularFontAwesomeModule } from 'angular-font-awesome';

// Import your library
import { OwlModule } from 'ngx-owl-carousel';

// Import SignaturePad
import { SignaturePadModule } from 'angular2-signaturepad';

// Import Form Module
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Import Webcam Module
import {WebcamModule} from 'ngx-webcam';

// Import Timer Module
import { NgxTimerModule } from 'ngx-timer';

// Import Chart Module
import { ChartsModule } from 'ng2-charts';


import { LoginflashscreenComponent } from './onboarding/loginflashscreen.component';
import { CongratulationsComponent } from './onboarding/congratulations.component';
import { VerificationComponent } from './onboarding/verification.component';
import { MobileVerificationComponent } from './onboarding/mobile-verification.component';
import { OtpScreenComponent } from './onboarding/otp-screen.component';
import { PanNumberComponent } from './onboarding/pan-number.component';
import { NgbdModalContent, MutualFundModalComponent } from './mutual-fund-modal/mutual-fund-modal.component';
import { PersonalDetailsComponent } from './onboarding/personal-details.component';
import { BankDetailsComponent } from './onboarding/bank-details.component';
import { KycVerificationComponent } from './onboarding/kyc-verification.component';
import { IfcCodeComponent } from './onboarding/ifc-code.component';
import { LoginComponent } from './login/login.component';
import { ResetpasswordComponent } from './login/resetpassword.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { SuccessComponent } from './account-details/success.component';
import { MandateComponent } from './account-details/mandate.component';
import { ChangepasswordComponent } from './account-details/changepassword.component';
import { SettingsComponent } from './account-details/settings.component';
import { FooterComponent } from './footer/footer.component';
import { ShareandearnComponent } from './account-details/shareandearn.component';
import { ReportComponent } from './account-details/report.component';
import { BankmandatedetailsComponent } from './account-details/bankmandatedetails.component';
import { OnetimemandateComponent } from './buying-flow/onetimemandate.component';
import { OtmuploadComponent } from './buying-flow/otmupload.component';
import { OrderhistoryComponent } from './order-history/orderhistory.component';
import { OrderhistorylumpsumComponent } from './order-history/orderhistorylumpsum.component';
import { PurchasesipComponent } from './buying-flow/purchasesip.component';
import { SipPaymentComponent } from './buying-flow/sip-payment.component';
import { SipSuccessComponent } from './buying-flow/sip-success.component';
import { SipBankSelectComponent } from './buying-flow/sip-bank-select.component';
import { OrderHistorySipComponent } from './order-history/order-history-sip.component';
import { EditSipComponent } from './order-history/edit-sip.component';
import { PortfolioInvestmentComponent } from './fund-portfolio/portfolio-investment.component';
import { FundTransactionComponent } from './fund-portfolio/fund-transaction.component';
import { FundfilterComponent } from './fund-filter/fundfilter.component';
import { ClickOutsideDirective } from './helpers/outerclick.directive';

/*services*/
import { PhonenumberService } from './onboarding/phonenumber.service';
import { AuthGuard } from './helpers/auth.guard';
import { loginAuthGuard } from './helpers/loginAuth.guard';
import { ImageService } from './helpers/image.service';
import { ApiServiceService } from './helpers/api-service.service';
import { RouterExtService } from './helpers/previous-router';
import { HelpComponent } from './account-details/help.component';
import { ViewdetailsComponent } from './account-details/viewdetails.component';

/*Custom Directive*/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberDirective } from './helpers/numbers-only.validator';
import { TwoDigitDecimaNumberDirective } from './helpers/numwithdecimal.validator';
import { alphaDirective } from './helpers/alpha-only.validator';
import { SignaturepadVerificationComponent } from './onboarding/signaturepad-verification.component';
import { PhotoComponent } from './onboarding/photo.component';
import { CameraComponent } from './camera/camera.component';
import { EmailComponent } from './onboarding/email.component';
import { SetPasswordComponent } from './onboarding/set-password.component';
import { HomePageComponent } from './home-page/home-page.component';
import { DatePipe } from '@angular/common';
import { FundDetailsComponent } from './buying-flow/fund-details.component';
import { SipDateComponent } from './buying-flow/sip-date.component';
import { SellScreenComponent } from './redemption/sell-screen.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { CheckUploadComponent } from './onboarding/check-upload.component';
import { Error404Component } from './error/error404.component';
import { Error500Component } from './error/error500.component';
import { FatcaDetailsComponent } from './onboarding/fatca-details.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginflashscreenComponent,
    CongratulationsComponent,
    VerificationComponent,
    MobileVerificationComponent,
    OtpScreenComponent,
    PanNumberComponent,
    MutualFundModalComponent,
    NgbdModalContent,
    PersonalDetailsComponent,
    BankDetailsComponent,
    KycVerificationComponent,
    NumberDirective,
    TwoDigitDecimaNumberDirective,
    alphaDirective,
    IfcCodeComponent,
    SignaturepadVerificationComponent,
    PhotoComponent,
    CameraComponent,
    LoginComponent,
    ResetpasswordComponent,
    AccountDetailsComponent,
    SuccessComponent,
    MandateComponent,
    ChangepasswordComponent,
    EmailComponent,
    SetPasswordComponent,
    SettingsComponent,
    HomePageComponent,
    HelpComponent,
    ViewdetailsComponent,
    FooterComponent,
    ShareandearnComponent,
    ReportComponent,
    BankmandatedetailsComponent,
    OnetimemandateComponent,
    OtmuploadComponent,
    FundDetailsComponent,
    OrderhistoryComponent,
    SipDateComponent,
    OrderhistorylumpsumComponent,
    PurchasesipComponent,
    SipPaymentComponent,
    SipSuccessComponent,
    SipBankSelectComponent,
    OrderHistorySipComponent,
    EditSipComponent,
    PortfolioInvestmentComponent,
    FundTransactionComponent,
    SellScreenComponent,
    FundfilterComponent,
    CheckUploadComponent,
    ClickOutsideDirective,
    Error404Component,
    Error500Component,
    FatcaDetailsComponent
  ],
  imports: [
    MbscModule,
    BrowserModule,
    SignaturePadModule,
    AppRoutingModule,
    OwlModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    WebcamModule,
    NgxTimerModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    TypeaheadModule.forRoot(),
    ChartsModule,
    BrowserAnimationsModule
  ],
  entryComponents: [NgbdModalContent],
  providers: [ loginAuthGuard, AuthGuard, DatePipe, PhonenumberService, ImageService, ApiServiceService, RouterExtService, InAppBrowser, Camera,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
