import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-mandate',
  templateUrl: './mandate.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./mandate.component.css']
})

export class MandateComponent implements OnInit {
  viewMode = 'tab1';
  public isCollapsed = true;
  public isCollapsed2 = true;
  activeFlag = false;
  loginflag = true;
  banklist: any;
  rowsControls = [];
  deVal: any = true;
  defaultval: number;
  mandate: any = true;
  loading: boolean;
  errorMessage: any;
  errorStatus: any;
  otherError: boolean;
  bank: any;
  closeResult: string;
  accNumber: any;
  modalReference: NgbModalRef;
  bankMandate: any;

  constructor(public router: Router, private apiService: ApiServiceService, private modalService: NgbModal, private errorHaldlingService: ErrorHaldlingService, public location: PlatformLocation) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });

    this.bankDetails();
  }

  ngOnInit() {

    //pushing two states in window.history inside ngOnInit(). Firstly the URL (/profile) I want to visit on the back key press and then the current URL (/show-bank).
    window.history.pushState({}, 'Account', '/profile');
    window.history.pushState({}, 'Bank-Mandate details', '/show-bank');

    // fetch bank details from localStorage
    let key = "bank_account"
    this.banklist = JSON.parse(localStorage.getItem(key));
    this.banklist.forEach(row => {
      this.rowsControls.push({
        isCollapsed: true
      })
    });
  }

  bankDetails(){
    /*get bank details*/
    this.apiService.getBankDetails().subscribe(
      res => {
        this.banklist = res.data.bank_details;
        this.bankMandate = res.data.mandate_all;
        this.banklist.forEach(row => {
          this.rowsControls.push({
            isCollapsed: true
          })
        });
        let key = "bank_account"
        localStorage.setItem(key, JSON.stringify(this.banklist));
        localStorage.setItem("allBankMandate", JSON.stringify(this.bankMandate));
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      
      }
    );
    /*get bank details*/

    // fetch bank details from localStorage
    let key = "bank_account"
    this.banklist = JSON.parse(localStorage.getItem(key));
    this.banklist.forEach(row => {
      this.rowsControls.push({
        isCollapsed: true
      })
    });
  }

  /*add Bank Account*/
  addBank(): void {
    let key = 'loginflag';
    this.router.navigate(['/ifsc-code'], { skipLocationChange: true });
    localStorage.setItem(key, JSON.stringify(this.loginflag));
  }
  /*add Bank Account*/

  addMandate(bank): void {
    // this.activeFlag = true;

    // let key = 'activeFlag';
    // localStorage.setItem(key, JSON.stringify(this.activeFlag));

    if (bank.mandate_details == null || bank.mandate_details.mandate_status == 'REJECTED') {
      var object = {};
      object['account_number'] = bank.account_number;
      object['bank_name'] = bank.name;
      object['mandate_flag'] = 'normal';

      this.router.navigate(['/one-time-mandate']);
      let key = 'account_details';
      localStorage.setItem(key, JSON.stringify(object));
    }
    else {
    }
  }

  // Function to open change default bank modal if bank length > 1
  changeDefaultModal(defBank, bank) {
    if (this.banklist.length > 1 && bank.default == 'N') {
      this.openVerticallyCentered(defBank, bank);
    }
  }

  openVerticallyCentered(defBank, bank) {
    this.accNumber = bank;
    this.modalReference = this.modalService.open(defBank, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop' })
    this.modalReference.result.then((result) => {
      if (result === 'Close click') {
        this.setDefault()
      }
    }, (reason) => {
      if (reason === ModalDismissReasons.ESC ||
        reason === ModalDismissReasons.BACKDROP_CLICK) {
        this.cancelDefault();
      }
    });;
  }

  /* Function if clicked Yes when changing banks */
  setDefault() {
    this.loading = true;

    var object = {};
    object['account_number'] = this.accNumber.account_number;
    this.apiService.setBankDefault(object).subscribe(
      res => {
        this.loading = false;
        this.defaultval = this.accNumber.id;
        this.deVal = false;
        this.banklist = res.data.bank_details;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
      }
    )
  }

  /* Function if clicked No/Modal Backdrop when changing banks */
  cancelDefault() {
    this.apiService.getBankDetails().subscribe(
      res => {
        this.loading = false;
        this.banklist = res.data.bank_details;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
      }
    );
  }

  /*view bank details function*/
  viewBankDetails(bank): void {
    this.loading = true;
    var object = {};
    object['account_number'] = bank.account_number;
    this.apiService.viewBankDetails(object).subscribe(
      res => {
        this.loading = false;
        let key = 'bankDetails';
        localStorage.setItem(key, JSON.stringify(res));
        let lenkey = 'bankLenth';
        localStorage.setItem(lenkey, JSON.stringify(this.banklist.length));
        this.router.navigate(['/view-bank-details']);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
        this.errorStatus = error.status;
        if(this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }
      }
    )
  }
  /*view bank details function*/

  /*mandate status function*/
  mandateStatus(bank): void {
    if (bank.mandate_details.mandate_flag != 1) {
      var object = {};
      object['account_number'] = bank.account_number;
      object['bank_name'] = bank.name;
      object['mandate_details_id'] = bank.mandate_details.id;

      this.router.navigate(['/one-time-mandate']);
      let accountkey = 'account_details';
      localStorage.setItem(accountkey, JSON.stringify(object));
      this.router.navigate(['/otm-upload']);
      let key = 'mandate_flag';
      localStorage.setItem(key, JSON.stringify(bank.mandate_details.mandate_flag));
    }
  }
  /*mandate status function*/

  public replace(content: string) {
    return content.replace(/.(?=(?:\D*\d){4})/g, "X");
  }
}


