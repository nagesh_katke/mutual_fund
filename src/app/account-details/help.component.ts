import { Component, OnInit } from '@angular/core';
import { mobiscroll, MbscPopupOptions } from '@mobiscroll/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})

export class HelpComponent implements OnInit {
  title: string;
  settings: MbscPopupOptions = {
    display: 'center',
    buttons: [{
      icon: 'close',
      handler: (event, inst) => {
        inst.hide();
      }
    },]
  };

  constructor(public location: Location) { }

  ngOnInit() {
  }

  support = [
    /*{
      "title": "SIP Related Questions",
      "popuptitle": "SIP Related Questions",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn2": "1. What is a SIP ?",
      "ans2": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn3": "1. What is a SIP ?",
      "ans3": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },
    {
      "title": "KYC Issues",
      "popuptitle": "KYC Issues",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn2": "1. What is a SIP ?",
      "ans2": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },
    {
      "title": "OTM (Mandate) Questions",
      "popuptitle": "OTM (Mandate) Questions",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn2": "1. What is a SIP ?",
      "ans2": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn3": "1. What is a SIP ?",
      "ans3": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },
    {
      "title": "Transaction Incomplete/Error",
      "popuptitle": "Transaction Incomplete/Error",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn2": "1. What is a SIP ?",
      "ans2": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn3": "1. What is a SIP ?",
      "ans3": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },
    {
      "title": "How and where to Invest",
      "popuptitle": "How and where to Invest",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn2": "1. What is a SIP ?",
      "ans2": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn3": "1. What is a SIP ?",
      "ans3": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },
    {
      "title": "Correction in Bank/KYC/Profile",
      "popuptitle": "Correction in Bank/KYC/Profile",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn2": "1. What is a SIP ?",
      "ans2": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
      "quesn3": "1. What is a SIP ?",
      "ans3": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },
    {
      "title": "ITR Filing",
      "popuptitle": "ITR Filing",
      "quesn1": "1. What is a SIP ?",
      "ans1": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates sint voluptatum nulla laborum, veniam velit. ",
    },*/
    {
      "title": "Contact to Support",
      "popuptitle": "Contact to Support",
      "quesn1": "1. You have any Queries?",
      "ans1": "For any queries you can reach our Trade Smart Expert by calling the Support Team on 022-61208000",
    }
  ];
}
