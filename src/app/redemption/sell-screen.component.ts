import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlatformLocation } from '@angular/common';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-sell-screen',
  templateUrl: './sell-screen.component.html',
  styleUrls: ['./sell-screen.component.css']
})

export class SellScreenComponent implements OnInit {
  submitted: boolean;
  modalReference: NgbModalRef;
  loading: boolean;
  sip_scheme: any = {};
  otherError: boolean;
  errorMessage: any;
  errorStatus: any;
  order: any;
  addnProp: any;
  entryExitData: any;
  mandateUpload: any;
  @ViewChild('sellRef') sellElementRef: ElementRef;
  viewExitLoad: any;
  sellUnit: any;
  sipamt: any;
  // numPattern = "^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$";
  numPattern = "^[1-9]{0,1}[0-9]+$";
  minRedeemAmt: any;
  sellAmtForm: FormGroup;
  unitsell = false;
  minRedeemstatus: boolean = false;
  zeroRedeemstatus: boolean;

  constructor(public location: PlatformLocation, private fb: FormBuilder, private modalService: NgbModal, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, private routerService: RouterExtService) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });

    const prev_url = this.routerService.getPreviousUrl();
    var flagsip = localStorage.getItem('reedm-success-flag');
    if(flagsip === 'true'){
      this.router.navigate(['/home']);
    }
  }

  ngAfterViewInit() {
    this.sellElementRef.nativeElement.focus();
  }

  ngOnInit() {
    this.order = JSON.parse(localStorage.getItem('sellFundDetails'));
    this.addnProp = JSON.parse(localStorage.getItem('addnProp'));
    this.minRedeemAmt = JSON.parse(localStorage.getItem('minRedeemAmt'));

    this.getExitLoad(this.order);

    this.sellAmtForm = this.fb.group({
      amount: [''],
    });
    // Amount required error validation 
    this.amountSell();
    if(this.order.redeemable_amount > this.minRedeemAmt.redemption_amount_minimum){
      this.minRedeemstatus = true;
    }
    else{
      this.minRedeemstatus = false;
    }

    if(this.order.redeemable_amount > 1){
      this.zeroRedeemstatus = true;
    }
    else{
      this.zeroRedeemstatus = false;
    }
    
    
  }
  
  
  // Function to get entry-exit load of scheme
  getExitLoad(input) {
    var object = {};
    object['isin'] = input.isin;

    this.apiService.getSchemeEntryExit(object).subscribe(
      res => {
        this.entryExitData = res.data;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setFieldValidators(error, this.sellAmtForm);
        this.errorStatus = error.status;
        if(this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 3000);
        }      
      }
    )
  }
  // convenience getter for easy access to form fields
  get f() { return this.sellAmtForm.controls; }

  amountSell() {
    const amountRequired = this.sellAmtForm.get('amount');
    if(this.unitsell){
      amountRequired.setValidators(null);
    }
    else if(this.minRedeemAmt.redemption_amount_minimum === 0){
      amountRequired.setValidators([Validators.required, Validators.pattern(this.numPattern), 
        Validators.max(this.order.redeemable_amount), 
        Validators.min(this.minRedeemAmt.redemption_amount_minimum = 1)]);
    }
    else {
      amountRequired.setValidators([Validators.required, Validators.pattern(this.numPattern), 
        Validators.max(this.order.redeemable_amount), 
        Validators.min(this.minRedeemAmt.redemption_amount_minimum)]);
    }
    amountRequired.updateValueAndValidity();
    this.sellAmtForm.updateValueAndValidity();
  }

  unitSell(): void {
    this.unitsell = !this.unitsell;
    this.amountSell();
  }

  // Functions to open modal

  openExitLoad(viewExitLoad) {
    this.modalReference = this.modalService.open(viewExitLoad, { windowClass: 'in submit-otm-upload', backdropClass: 'change-modal-backdrop' });
  }

  openSellFund(sellFund) {
    this.modalReference = this.modalService.open(sellFund, { windowClass: 'in submit-otm-upload', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }
  sellAmount(): void {
    /*Sell function*/
    this.submitted = true;
    if (this.sellAmtForm.valid) {

      var object = {};
      const amountFormdata = JSON.stringify(this.sellAmtForm.value);
      const userAmount = JSON.parse(amountFormdata);
      // let frequency_type = 'frequency_type';
      // let type = localStorage.getItem(frequency_type);
      this.loading = true;
      this.sip_scheme['order_type'] = "redeem";
      this.sip_scheme['scheme_code'] = this.order.scheme_code;
      this.sip_scheme['scheme_name'] = this.order.scheme_name;
      this.sip_scheme['frequency_type'] = 'normal';
      if(this.order.sip_frequency != 'MONTHLY'){
        this.sip_scheme['frequency_flag'] = 'N';
      }
      else{
        this.sip_scheme['frequency_flag'] = 'M';
      }
      this.sip_scheme['folio_no'] = this.addnProp.folio_no;
      this.sip_scheme['redeem_by'] = "amount";
      if (this.unitsell) {
        // this.sip_scheme['redeem_by'] = "all";
        this.sip_scheme['amount'] = parseInt(this.order.redeemable_amount);
        // this.sip_scheme['amount'] = this.order.redeemable_amount;
        // this.sip_scheme['unit'] = this.order.total_units;
        this.sellUnit = this.order.total_units;
      }
      else {
        this.sip_scheme['amount'] = userAmount.amount;
        this.sellUnit = userAmount.amount / this.order.current_nav;

      }

      // let sip_info = 'scheme_info';

      this.apiService.proceedSip(this.sip_scheme).subscribe(
        res => {
          let element: HTMLElement = document.getElementById('proceedSipBtn') as HTMLElement;
          localStorage.setItem('reedm-success-flag', 'true');
          element.click();
          this.loading = false;

          // this.openMandateUpload(mandateUpload);
        },
        (error) => {
          this.loading = false;
        this.errorHaldlingService.setFieldValidators(error, this.sellAmtForm);
        this.errorStatus = error.status;
        if(this.errorStatus != "400") {
          this.otherError = true;
          this.errorMessage = error.error.data;
          setTimeout(function () {
            this.otherError = false;
          }.bind(this), 5000);
        }
          if (this.errorStatus == "400") {
            this.loading = false;
            const validationErrors = error.error.data;

            Object.keys(validationErrors).forEach(prop => {
              const formControl = this.sellAmtForm.get(prop);
              if (formControl) {
                // activate the error message
                formControl.setErrors({
                  serverError: validationErrors[prop]
                });
              }
            });
          }
          else {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 5000);
          }
        }
      )
    }
    /*sell function*/
  }

  orderHistory(): void {
    this.modalReference.close();
    this.router.navigate(['/order-history']);
  }

}
