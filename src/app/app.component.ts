import { Component, NgZone } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { Router } from '@angular/router';
import { ApiServiceService } from './helpers/api-service.service';
import { environment } from 'src/environments/environment.prod';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mutualfund';

  status = 'ONLINE'; //initializing as online by default
  isConnected = true;
  Nointernet: boolean = false;
  updateAvailable: boolean;
  currentVersion: any = environment.currentVersion;
  updateLater: boolean;

  constructor(private connectionService:ConnectionService, private apiService: ApiServiceService, private zone: NgZone, private router: Router
    ) {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if(this.isConnected){
      this.status = "ONLINE";
      } else {
      this.status = "OFFLINE";
      this.Nointernet = true;
      }
      })

      this.router.routeReuseStrategy.shouldReuseRoute = function(){
        return false;
 }

     this.checkVersionUpdate();
  
   }



   
retry():void{
  
  if(this.status != "OFFLINE"){
    this.router.navigated = false;
    this.router.navigate([this.router.url]);
       this.Nointernet = false;
      
  }
}


/*check update version*/
 checkVersionUpdate(){
       let res = this.apiService.getVerison().subscribe(
         res  => {
             if(this.currentVersion != res.data[0].version_no){                     
                 this.updateAvailable = true;
                 
             }
             if(res.data[0].type == 'N'){
              this.updateLater = true;
             }
         }
     )
  }
  
  appUpdate(){
  window.open("https://play.google.com/store/apps/details?id=com.tsoliteuat.lite&ah=z9l35hrtplO0DOft8yWbX592aBw ", "_blank");
  }
  
  appUpdateClose(){
    this.updateAvailable = false;
  }


}




