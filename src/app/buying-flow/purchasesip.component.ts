import { Component, ViewChild, OnInit, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-purchasesip',
  templateUrl: './purchasesip.component.html',
  styleUrls: ['./purchasesip.component.css']
})

export class PurchasesipComponent implements OnInit {
  sipamount: any;
  submitted = false;
  sipAmtForm: FormGroup;
  sipamt: any;
  minmaxamt: any;
  loading: boolean;
  investment_type: any;
  modalReference: NgbModalRef;
  funds: any;
  otherError: boolean;
  errorMessage: any;
  sip_scheme: any = {};
  sipdayflag: boolean;
  minamount: any;
  maxamount: any;
  errorStatus = '';
  numPattern = "^[1-9]{0}[0-9]+$";
  @ViewChild('purchaseRef') purchaseElementRef: ElementRef;

  constructor(private fb: FormBuilder, private router: Router, private cdr: ChangeDetectorRef, private apiService: ApiServiceService, private modalService: NgbModal, private errorHaldlingService: ErrorHaldlingService, public location: PlatformLocation) { 
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngAfterViewInit() {
    this.purchaseElementRef.nativeElement.focus();
  }


  ngOnInit() {
    /*investment type*/
    let investment_key = 'investment_type';
    this.investment_type = localStorage.getItem(investment_key);
    /*investment type*/

    /*min amount*/
    let key = 'scheme_amount';
    this.minmaxamt = JSON.parse(localStorage.getItem(key));

    if (this.investment_type == 'lumsum') {
      this.minamount = this.minmaxamt.minimum_purchase_amount;
      if (this.minmaxamt.maximum_purchase_amount === 0) {
        this.maxamount = '';
      }
      else {
        this.maxamount = this.minmaxamt.maximum_purchase_amount;
      }
    }
    else {
      this.minamount = this.minmaxamt.sip_minimum_installment_amount;
      this.maxamount = this.minmaxamt.sip_maximum_installment_amount;
      if (this.minmaxamt.sip_maximum_installment_amount === 0) {
        this.maxamount = '';
      }
      else {
        this.maxamount = this.minmaxamt.sip_maximum_installment_amount;
      }
    }

    /*min amount*/
    this.sipAmtForm = this.fb.group({
      investSipAmt: ['', [Validators.required,Validators.pattern(this.numPattern), Validators.min(this.minamount), Validators.max(this.maxamount)]]
    });

    /*fund details*/
    let fundkey = 'scheme_details';
    this.funds = JSON.parse(localStorage.getItem(fundkey));
    /*fund details*/
  }

  // convenience getter for easy access to form fields
  get f() { return this.sipAmtForm.controls; }

  getOtmAmount(innerText) {
    this.sipamount = innerText.replace(/\,/g, "");
    this.sipamt = JSON.parse(this.sipamount);
    this.cdr.detectChanges();
  }

  /*save sip function*/
  saveSip(selectSip): void {
    this.submitted = true;
    if (this.sipAmtForm.valid) {
      if (this.investment_type == 'lumsum') {
        this.modalReference =  this.modalService.open(selectSip, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
      }
      else {
        var object = {};
        const amountFormdata = JSON.stringify(this.sipAmtForm.value);
        const userAmount = JSON.parse(amountFormdata);

        this.loading = true;
        object['scheme_code'] = this.funds.is_sip.scheme_code;
        this.apiService.getMFDates(object).subscribe(
          res => {
            this.loading = false;
            let key = 'scheme_dates';
            localStorage.setItem(key, JSON.stringify(res.data));
            let flagkey = 'sipdate';
            localStorage.setItem(flagkey, JSON.stringify('true'));
            let frequency_type = 'frequency_type';
            let type = localStorage.getItem(frequency_type);
            var scheme_info = {};
            scheme_info['scheme_code'] = this.funds.is_sip.scheme_code;
            scheme_info['amount'] = userAmount.investSipAmt;
            scheme_info['frequency_type'] = type;
            let sip_info = 'scheme_info';
            localStorage.setItem(sip_info, JSON.stringify(scheme_info));
            // let keysToRemove = ["frequency_type", "sip_amount"];
            // keysToRemove.forEach(k => localStorage.removeItem(k));
            this.router.navigate(['/sip-date']);
          },
          (error) => {
            this.loading = false;
            this.errorHaldlingService.setFieldValidators(error, this.sipAmtForm);
            this.errorStatus = error.status;
            if(this.errorStatus != "400"){
              this.otherError = true;
              this.errorMessage = error.error.data;
              setTimeout(function() {
                this.otherError = false;
            }.bind(this), 3000);
            }
          });
      }
    }
  }
  /*save sip function*/

  /*pay lumsum function*/
  investLumsum(): void {
    this.sipdayflag = true;
    this.submitted = true;
    if (this.sipAmtForm.valid) {
      var object = {};
      const amountFormdata = JSON.stringify(this.sipAmtForm.value);
      const userAmount = JSON.parse(amountFormdata);
      let frequency_type = 'frequency_type';
      let type = localStorage.getItem(frequency_type);
      this.loading = true;
      this.sip_scheme['order_type'] = "purchase";
      this.sip_scheme['scheme_code'] = this.funds.is_sip.scheme_code;
      this.sip_scheme['amount'] = userAmount.investSipAmt.trim();
      this.sip_scheme['frequency_type'] = type;
      this.sip_scheme['scheme_name'] = this.funds.fund_details.SchemeName;
      let sip_info = 'scheme_info';
      localStorage.setItem(sip_info, JSON.stringify(this.sip_scheme));

      this.apiService.proceedSip(this.sip_scheme).subscribe(
        res => {
          this.loading = false;
          let sip_order = 'order_id';
          localStorage.setItem(sip_order, res.data.order_id);
          this.router.navigate(['/sip-payment']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.sipAmtForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400"){
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function() {
              this.otherError = false;
          }.bind(this), 3000);
          }
        }
      )
    }
    /*pay lumsum function*/
  }

  cancelSipday(): void {
    this.sipdayflag = false;
  }
}

