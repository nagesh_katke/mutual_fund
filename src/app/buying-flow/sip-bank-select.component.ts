import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-sip-bank-select',
  templateUrl: './sip-bank-select.component.html',
  styleUrls: ['./sip-bank-select.component.css']
})
export class SipBankSelectComponent implements OnInit {
  newMandateForm: FormGroup;
  bank: any;
  submitted = false;
  banklist: [];
  sipMandateFlag = true;
  errorStatus = '';
  loading: boolean;

  constructor(public router: Router, public location: Location, private fb: FormBuilder, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService ) { 
    this.bankGetDetails();
  }

  ngOnInit() {
    this.newMandateForm = this.fb.group({
      bankSelect: ['', [Validators.required]],
    });

  }

  bankGetDetails() {
      /*get bank detalis*/
    this.apiService.getBankDetails().subscribe(
      res => {
        this.banklist = res.data.bank_details;
        this.banklist.forEach(row => {
        });
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    );
    /*get bank detalis*/
  }

  // convenience getter for easy access to form fields
  get f() { return this.newMandateForm.controls; }

  // Function to add new mandate
  addNewMandate(bank): void {
    this.submitted = true;
    const bank_id = JSON.stringify(this.newMandateForm.value);
    const bankValue = JSON.parse(bank_id);

    if (this.newMandateForm.valid) {
      var object = {};
      object['account_number'] = bankValue.bankSelect.account_number;
      object['bank_name'] = bankValue.bankSelect.name;
      object['sip_mandate_flag'] = this.sipMandateFlag;
      object['mandate_flag'] = 'sip';

      this.router.navigate(['/one-time-mandate']);
      let key = 'account_details';
      localStorage.setItem(key, JSON.stringify(object));
    }

  }

}
