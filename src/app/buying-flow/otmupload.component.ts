import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { PlatformLocation } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-otmupload',
  templateUrl: './otmupload.component.html',
  styleUrls: ['./otmupload.component.css']
})

export class OtmuploadComponent implements OnInit {
  closeResult: string;
  modalReference: NgbModalRef;
  bankdetails: any;
  mandate_flag: any;
  public showGallary = false;
  loading: boolean;
  resend = false;
  errorStatus: any;
  capturedSnapURL: string;

  cameraOptions: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    sourceType: this.camera.PictureSourceType.CAMERA,
    mediaType: this.camera.MediaType.PICTURE
  }
  mandate_id: any;

  constructor(private modalService: NgbModal, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: PlatformLocation, private camera: Camera) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }

  ngOnInit() {
    let key = 'account_details';
    this.bankdetails = JSON.parse(localStorage.getItem(key));
    console.log(this.bankdetails);
    
    let mandate_key = 'mandate_flag';
    this.mandate_flag = JSON.parse(localStorage.getItem(mandate_key));

    let mandate_id = 'mandate_flag_key';
    this.mandate_id = localStorage.getItem(mandate_id);
    console.log(this.mandate_id);
    
  }

  /* Functions to open different modals */
  mandateSignInstruction(sign) {
    this.modalReference = this.modalService.open(sign, { centered: true, windowClass: 'in otm-upload-custom-class', backdropClass: 'change-modal-backdrop' });
  }

  openUpload(upload) {
    this.modalReference = this.modalService.open(upload, { windowClass: 'in select-otm-upload', backdropClass: 'change-modal-backdrop' });
  }

  openMandateUpload(mandateUpload) {
    this.modalReference = this.modalService.open(mandateUpload, { windowClass: 'in submit-otm-upload', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }

  resendMandate(resend) {
    this.modalReference = this.modalService.open(resend, { centered: true, windowClass: 'in', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }
  /* Functions to open different modals */

  /*Resend Email*/
  resendEmail(): void {
    this.loading = true;
    var object = {};
    object['mandate_details_id'] = this.bankdetails.mandate_details_id;
    this.apiService.resendMandate(object).subscribe(
      res => {
        // this.resend = true;
        this.loading = false;
        let element: HTMLElement = document.getElementById('mfMail-send') as HTMLElement;
        element.click();
        setTimeout(function () {
          // ensure that modal is opened
            this.modalReference.close();
        }.bind(this), 3000);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }

    );
  }


  /*Image Upload*/
  fileToUpload: File = null;
  imageUrl: string = "";
  /*file upload*/
  processFile(file: FileList) {
    this.fileToUpload = file.item(0);
    //show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    this.showGallary = true;
    this.modalReference.close();
  }

  /*Submit Mandate*/
  submitMandate(): void {
    this.loading = true;
    var object = {};
    
      object['mandate_details_id'] = this.bankdetails.mandate_details_id;
        object['singed_mandate'] = this.imageUrl;
    this.apiService.uploadMandate(object).subscribe(
      res => {
        this.loading = false;
        let element: HTMLElement = document.getElementById('otm-mandate') as HTMLElement;
        element.click();
        localStorage.removeItem('mandate_flag_key');
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    );
  }
  /*Submit Mandate*/

  orderHistory(): void {
    this.modalReference.close();
    this.router.navigate(['/order-history']);
  }

  bankGo(): void {
    this.modalReference.close();
    this.router.navigate(['/show-bank']);
  }

  takeSnap() {
    this.camera.getPicture(this.cameraOptions).then((imageData) => {
      // this.camera.DestinationType.FILE_URI gives file URI saved in local
      // this.camera.DestinationType.DATA_URL gives base64 URI
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imageUrl = base64Image;
      this.showGallary = true;
      this.modalReference.close();
    }, (err) => {

      console.log(err);
      // Handle error
    });
  }

}
