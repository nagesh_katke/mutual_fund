import { Component, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-sip-success',
  templateUrl: './sip-success.component.html',
  styleUrls: ['./sip-success.component.css']
})

export class SipSuccessComponent implements OnInit {
  public isViewable: boolean;
  loading: boolean;
  otherError: boolean;
  banklist: [];
  modalReference: NgbModalRef;
  modalRef: NgbModalRef;
  bank: any;
  errorMessage: any;
  errorStatus: any;
  funds: any;
  rowsControls = [];
  bankname: void;
  prev_url: string;
  allMandate: any;

  constructor(public router: Router, public location: PlatformLocation, private modalService: NgbModal, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, private routerService: RouterExtService) {
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
        this.modalRef.close();
      }
    });
    const prev_url = this.routerService.getPreviousUrl();
    console.log(prev_url);
    var flagsip = localStorage.getItem('sip-successfl-flag');
    if((prev_url != "/sip-date" && prev_url != "/sip-payment"  && prev_url != "/sip-bank-select" && prev_url != 'sip-bank-select') && flagsip != 'false'){
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.isViewable = true;

    /*fund details*/
    let fundkey = 'scheme_details';
    this.funds = JSON.parse(localStorage.getItem(fundkey));

    this.allMandate = JSON.parse(localStorage.getItem("allBankMandate"))

    /*fund details*/

    // fetch bank details from localStorage
    let key = "bank_account"
    this.banklist = JSON.parse(localStorage.getItem(key));
    this.banklist.forEach(row => {
      this.rowsControls.push({
        isCollapsed: true
      })
    });

  }

  // Function to open modal 
  automateSip(automate) {
    this.modalReference = this.modalService.open(automate, { windowClass: 'in select-otm-upload', backdropClass: 'change-modal-backdrop' });
    this.modalReference.result.then((result) => {
      if (result === 'Close click') {
        this.toggle()
      }
    }, (reason) => {
      if (reason === ModalDismissReasons.ESC ||
        reason === ModalDismissReasons.BACKDROP_CLICK) {
        this.toggle()
      }
    });;
  }

  // Function to add new mandate
  newMandate(): void {
    this.loading = true;
    this.modalReference.close();
    this.router.navigate(['/sip-bank-select']);
  }

  // Function to select mandate for SIP
  mandateForSip(bank): void {
    if (bank.mandate_details != null) {
      var object = {};
      object['order_id'] = localStorage.getItem('order_id');
      object['mandate_id'] = bank.mandate_details.id;
      this.bankname = bank.name;
      this.loading = true;
      this.modalReference.close();

      this.apiService.getUpdatedMandateId(object).subscribe(
        res => {
          this.loading = false;
          let element: HTMLElement = document.getElementById('sip-mandate') as HTMLElement;
          element.click();
          setTimeout(function () {
            // ensure that modal is opened
            if (this.modalRef !== undefined) {
              this.modalRef.close();
            }
            localStorage.setItem('sip-successfl-flag', 'true');
            this.router.navigate(['/order-history']);
          }.bind(this), 5000);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);

        }
      )
    }

  }

  /** Simple method to toggle element visibility */
  public toggle(): void { this.isViewable = !this.isViewable; }

  /*Existing mandate selected Modal*/
  openSipMandate(sipMandate) {
    this.modalRef = this.modalService.open(sipMandate, { centered: true, windowClass: 'in remove-bank-popup', backdropClass: 'change-modal-backdrop', backdrop: 'static', keyboard: false });
  }
  /*Existing mandate selected Modal*/

}
