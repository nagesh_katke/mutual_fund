import { Component, OnInit, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { Location, DatePipe } from '@angular/common';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { MultiDataSet, Label, Color, BaseChartDirective } from 'ng2-charts';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-fund-details',
  templateUrl: './fund-details.component.html',
  styleUrls: ['./fund-details.component.css']
})

export class FundDetailsComponent implements OnInit {
  fundetails: [];
  funds: any;
  date: any;
  loading: boolean;
  chartLoading: boolean;
  errorMessage: any;
  otherError: boolean;
  status: any;
  investmentType: any;
  errorStatus = '';
  newFundCompany = [];
  newFundpercentage = [];
  newFundDetails = [];
  month: any = [];
  price: any = [];
  navdate: any;
  chart: any;
  fund_isin: any;
  selectedItem: any;
  is_sip: any;
  chartDataMax: any;
  chartDataOneMonth: any;
  chartDataThreeMonth: any;
  chartDataOneYear: any;
  chartDataThreeYear: any;
  chartDataFiveYear: any;
  modalReference: NgbModalRef;
  percentAmount: any;
  schemeAssetSize: any;
  funsectordata: any;
  fundres: boolean = false;

  constructor(public location: PlatformLocation, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, private modalService: NgbModal,  public datepipe: DatePipe) {
    let key = 'scheme_details';
    this.funds = JSON.parse(localStorage.getItem(key));
    localStorage.setItem('scheme_amount', JSON.stringify(this.funds.min_max_investment_data));

    this.getFundSector();
    location.onPopState((event) => {
      // ensure that modal is opened
      if (this.modalReference !== undefined) {
        this.modalReference.close();
      }
    });
  }
  public lineChartData: ChartDataSets[] = [
    { data: this.price, label: 'Nav RS' }
  ];


  public monthPeriod: any = [
    { period: '1M', name: '1M' },
    { period: '3M', name: '3M' },
    { period: '1Y', name: '1Y' },
    { period: '3Y', name: '3Y' },
    { period: '5Y', name: '5Y' },
    // { period: ' ', name: 'MAX' },
  ];

  public lineChartLabels: Label[] = this.month;
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    tooltips: {
      borderColor: '#fff',
      backgroundColor: '#fff',
      bodyFontColor: '#000',
      titleFontColor: '#000'
    },
    legend: {
      display: false
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        display: false //this will remove all the x-axis grid lines
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          gridLines: {
            color: '#e5e5e5',
          },
          ticks: {
            fontColor: '#777677',
          }
        }
      ]
    },
    annotation: {
      annotations: [],
    },
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(211,233,217,1)',
      borderColor: '#1f9174',
      borderWidth: 1,
      pointBackgroundColor: '#1f9174',
      pointBorderColor: '#1f9174',
      pointRadius: 1.2,
      pointHoverBackgroundColor: '#1f9174',
      pointHoverBorderColor: '#1f9174'
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];
  /*chart inputs*/

  viewMode = 'tab1';

  /*Start Nav Chart Data*/
  getNavmapDetails(navdate): void {
    this.chartDataOneMonth = JSON.parse(localStorage.getItem("chartonemonth"));
    this.chartDataThreeMonth = JSON.parse(localStorage.getItem("chartthreemonth"));
    this.chartDataOneYear = JSON.parse(localStorage.getItem("chartoneyear"));
    this.chartDataThreeYear = JSON.parse(localStorage.getItem("chartthreeyear"));
    this.chartDataFiveYear = JSON.parse(localStorage.getItem("chartfiveyear"));
    // this.chartDataMax = JSON.parse(localStorage.getItem("chartdatamax"));
    this.selectedItem = navdate;
    let object = {};
    let isinkey = 'scheme_isin';
    this.fund_isin = JSON.parse(localStorage.getItem(isinkey));
    object['isin'] = this.fund_isin;
    object['time_period'] = navdate;
    this.month = [];
    this.price = [];

    if (this.chartDataMax != null && navdate == ' ') {
      this.chartDataMax.forEach(y => {
        this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
        this.price.push(y.NAVRS);
      });

      this.chartLoading = false;
      this.lineChartData = [
        { data: this.price, label: 'Nav Rs' }
      ];
      this.lineChartLabels = this.month;
    }

    else if (this.chartDataOneMonth != null && navdate == '1M') {
      this.chartDataOneMonth.forEach(y => {
        this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
        this.price.push(y.NAVRS);
      });

      this.chartLoading = false;
      this.lineChartData = [
        { data: this.price, label: 'Nav Rs' }
      ];
      this.lineChartLabels = this.month;
    }
    else if (this.chartDataThreeMonth != null && navdate == '3M') {
      this.chartDataThreeMonth.forEach(y => {
        this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
        this.price.push(y.NAVRS);
      });

      this.chartLoading = false;
      this.lineChartData = [
        { data: this.price, label: 'Nav Rs' }
      ];
      this.lineChartLabels = this.month;
    }
    else if (this.chartDataOneYear != null && navdate == '1Y') {
      this.chartDataOneYear.forEach(y => {
        this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
        this.price.push(y.NAVRS);
      });

      this.chartLoading = false;
      this.lineChartData = [
        { data: this.price, label: 'Nav Rs' }
      ];
      this.lineChartLabels = this.month;
    }
    else if (this.chartDataThreeYear != null && navdate == '3Y') {
      this.chartDataThreeYear.forEach(y => {
        this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
        this.price.push(y.NAVRS);
      });

      this.chartLoading = false;
      this.lineChartData = [
        { data: this.price, label: 'Nav Rs' }
      ];
      this.lineChartLabels = this.month;
    }
    else if (this.chartDataFiveYear != null && navdate == '5Y') {
      this.chartDataFiveYear.forEach(y => {
        this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
        this.price.push(y.NAVRS);
      });

      this.chartLoading = false;
      this.lineChartData = [
        { data: this.price, label: 'Nav Rs' }
      ];
      this.lineChartLabels = this.month;
    }

    else {
      this.chartLoading = true;
       this.apiService.getNavChart(object).subscribe(
        res => {
          if (navdate == ' ') {
            localStorage.setItem("chartdatamax", JSON.stringify(res.data))
          }
          else if (navdate == '1M') {
            localStorage.setItem("chartonemonth", JSON.stringify(res.data))
          }
          else if (navdate == '3M') {
            localStorage.setItem("chartthreemonth", JSON.stringify(res.data))
          }
          else if (navdate == '1Y') {
            localStorage.setItem("chartoneyear", JSON.stringify(res.data))
          }
          else if (navdate == '3Y') {
            localStorage.setItem("chartthreeyear", JSON.stringify(res.data))
          }
          else if (navdate == '5Y') {
            localStorage.setItem("chartfiveyear", JSON.stringify(res.data))
          }
          res.data.forEach(y => {
            this.month.push(this.datepipe.transform(y.NAVDATE, 'dd-MM-yyyy'));
            this.price.push(y.NAVRS);
          });
          this.chartLoading = false;
          this.lineChartData = [
            { data: this.price, label: 'Nav Rs' }
          ];
          this.lineChartLabels = this.month;

        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
          this.errorStatus = error.status;
          if (this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
  }
  /*End Nav Chart Data*/

  /*Fund Sector Data*/
  getFundSector():void{
    var object = {};
    object['isin'] = this.funds.is_sip.isin;
    object['scheme_code'] = this.funds.is_sip.scheme_code;
    this.apiService.getFundallocation(object).subscribe(
      res => {
        this.funsectordata = res.data;
        this.fundres = true;
      for (let i = 0; i <= this.funsectordata.fund_concentration[0].length - 1; i++) {
      let object = {};
      object['ASECT_TYPE'] = this.funsectordata.fund_concentration[0][i].ASECT_TYPE;
      object['HOLDPERCENTAGE'] = this.funsectordata.fund_concentration[0][i].Holdpercentage;
      object['color'] = this.bgcolor[i];
      this.newFundDetails.push(object);
    }
        if (this.funsectordata.fund_concentration[0].length != "0") {
      for (let i = 0; i <= this.funsectordata.fund_concentration[0].length - 1; i++) {
        let object;
        object = this.funsectordata.fund_concentration[0][i].ASECT_TYPE;
        this.newFundCompany.push(object);
      }
      for (let i = 0; i <= this.funsectordata.fund_concentration[0].length - 1; i++) {
        let object;
        object = this.funsectordata.fund_concentration[0][i].Holdpercentage;
        this.newFundpercentage.push(parseFloat(object).toFixed(2));
      }
      this.schemeAssetSize = (this.funsectordata.fund_concentration[2].AUM / 100).toFixed(2);
    }

      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }
  /*Fund Sector Data*/

  ngOnInit() {
    this.getNavmapDetails('1M');
    this.date = this.funds.nav_date;
    this.updatePLdoughnut();

    // To get fund min-max amt
    // this.percentAmount = this.funds.fund_details.NetAsset;
   this.percentAmount = (this.funds.fund_details.NetAsset / 100).toFixed(2);
    // this.peValue = (this.funds.fund_details.PE).toFixed(2);
  }

  // Functions to open modal
  disclaimerText(content) {
    this.modalReference = this.modalService.open(content, { centered: true, windowClass: 'in otm-upload-custom-class', backdropClass: 'change-modal-backdrop' });
  }

  // Doughnut Chart
  public doughnutChartOptions: any = {
    responsive: true,
    tooltips: {
      borderColor: '#fff',
      backgroundColor: '#fff',
      bodyFontColor: '#000'
    },
    legend: {
      display: false,
      labels: {
        display: false
      }
    }
  };

  public bgcolor = ['#4671bf', '#26d6fb', '#c3fa60', '#56c374', '#1b9bfe', '#26d6d6', '#c3fafa', '#56c3c3', '#1b9b9b', '#C0C0C0', '#808080', '#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#00FFFF', '#008080', '#0000FF', '#D6EAF8', '#73C6B6', '#EC7063', '#AED6F1', '#6E2C00', '#D7BDE2'];
  public doughnutChartLabels: Label[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales', 'Mail-Order Sales1', 'Mail-Order Sales2'];
  public doughnutChartData: MultiDataSet = [
    [350, 450, 100, 100, 400, 100],
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public chartColors: Array<any> = [
    { // all colors in order
      backgroundColor: this.bgcolor
    }
  ]


  public updatePLdoughnut() {
    this.doughnutChartLabels = this.newFundCompany;
    this.doughnutChartData = this.newFundpercentage;
  }
  // Doughnut Chart


  /*monthly SIP*/
  monthlySip(type): void {
    this.investmentType = type;
    /*account status*/
    let key = 'account_status';
    this.status = localStorage.getItem(key);
    /*account status*/

    /*fund status*/
    let fund_key = 'fund_status';
    localStorage.setItem(fund_key, 'true');
    /*fund status*/

    var pagestatus = this.status;

    switch (pagestatus) {
      case "10": {
        this.router.navigate(['/mobile-verification']);
        break;
      }

      case "20": {
        this.router.navigate(['/pan-number']);
        break;
      }

      case "30": {
        this.router.navigate(['/personal-details']);
        break;
      }

      case "35": {
        this.router.navigate(['/fatca-details']);
        break;
      }

      case "40": {
        this.router.navigate(['/ifsc-code']);
        break;
      }

      case "50": {
        this.router.navigate(['/signaturepad-verification']);
        break;
      }
      default: {
        localStorage.removeItem(fund_key);

        let investmemnt_type = 'frequency_type';
        if (this.investmentType == 'lumsum') {
          localStorage.setItem(investmemnt_type, 'NORMAL');
        }
        else {
          localStorage.setItem(investmemnt_type, 'MONTHLY');
        }
        let investmemnt = 'investment_type';
        localStorage.setItem(investmemnt, this.investmentType);
        this.router.navigate(['/purchase-sip']);
        break;
      }
    }

  }
  /*monthly SIP*/

  ngOnDestroy() {
    localStorage.removeItem("chartonemonth");
    localStorage.removeItem("chartthreemonth");
    localStorage.removeItem("chartoneyear");
    localStorage.removeItem("chartthreeyear");
    localStorage.removeItem("chartfiveyear");
    localStorage.removeItem("chartdatamax");
    }
  }
