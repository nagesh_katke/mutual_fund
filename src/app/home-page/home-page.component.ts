import { Component, OnInit, ViewChild, ElementRef, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// import {NgbTypeahead, NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, catchError, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ApiServiceService } from '../helpers/api-service.service';
import { TypeaheadMatch, TypeaheadDirective } from 'ngx-bootstrap';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

export class HomePageComponent implements OnInit {
 @ViewChild('typeahead') typeahead: TypeaheadDirective;
  showSearchBox: boolean;
  ifsearchForm: FormGroup;
  riskForm: FormGroup;
  schemeByCategory: FormGroup;
  schemeByInvestment: FormGroup;
  public fundsname: any;
  public model: any;
  loading = false;
  fundname = {};
  topFund: [];
  selectedValue: string;
  selectedOption: any;
  public states: any;
  funds: any;
  noResult = false;
  errorMessage: any;
  errorStatus: any;
  otherError: boolean;
  @ViewChild('homeRef') homeElementRef: ElementRef;

  productName: any;
  public fundslist: any;
  banklist: any;
  rowsControls = [];
  prev_ur: string;
  bankMandate: any;

  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, private routerService: RouterExtService) { 
    this.fundsName();
    this.detailsUser();
    this.topFunds();
    this.personalDetails();
    this.bankDetails();
}

  /*carousel opt*/
  carouselOptions = {
    margin: 0,
    nav: true,
    loop: true,
    // autoplay:true,
    // autoplayTimeout:2000,
    navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1.2
      },
      600: {
        items: 1.2
      },
      1000: {
        items: 2
      },
      1500: {
        items: 2
      }
    }
  }

  images = [
    {
      text: "Invest to Save Tax", image: "../assets/images/invest-to-save-tax.svg", value: "tax-saver",
    },
    {
      text: "Better than Saving Account", image: "../assets/images/better-than-savings-account.svg", value: "savings",
    },
    {
      text: "Invest in Large Companies", image: "../assets/images/invest-in-large.svg", value: "large-inv",
    },
    {
      text: "Better than Fixed Deposit", image: "../assets/images/better-than-fixed-deposits.svg", value: "bt-fd",
    }
  ]
  /*carousel opt*/

  public isCollapsed = true;
  viewMode = 'tab1';

  // @ViewChild('instance', { }) instance: NgbTypeahead;
  // focus$ = new Subject<string>();
  // click$ = new Subject<string>();

  ngOnInit() {
    const prev_url = this.routerService.getPreviousUrl();    
    if(prev_url === "/order-history-sip" || prev_url === "/sip-success" || prev_url === "/sip-payment" ){
      this.router.navigate(['/home']);
  }

    this.ifsearchForm = this.fb.group({
      bankName: ['']
    });

    /*Extract Fund list*/
    let key = 'fund_list';
    this.fundslist = localStorage.getItem(key);
    this.fundsname = JSON.parse(this.fundslist);
    this.states = this.fundsname;
    /*Extract Fund list*/

    // Form created To fetch value from input fields divs
    this.riskForm = this.fb.group({
      risk: ['',],
    }
    );

    this.schemeByCategory = this.fb.group({
      fundByCategory: ['',],
    }
    );

    this.schemeByInvestment = this.fb.group({
      fundByInvestment: ['',],
    }
    );
    // Form created To fetch value from input fields divs

  }

  fundsName(){
    if (this.fundsname == null) {
      this.apiService.getFundName().subscribe(
        res => {
          this.fundsname = res.data;
          this.states = this.fundsname;
          let fund_list = 'fund_list';
          localStorage.setItem(fund_list, JSON.stringify(this.fundsname));
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
        }
      )
    }
  }

  detailsUser(){
    this.apiService.getUserdetails().subscribe(
      res => {
        let account_status = 'account_status';
        localStorage.setItem(account_status, res.data.account_status);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      });
  }

  topFunds(){
    // To get Top Performing Funds
    this.apiService.getTopFunds().subscribe(
      res => {
        this.topFund = res.data;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  personalDetails(){
    // To store account details and display in account details page
    this.apiService.getPersonaldetails().subscribe(
      res => {
        let key = 'acc_details'
        localStorage.setItem(key, JSON.stringify(res.data));
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    );
  }

  bankDetails(){
    /*get bank details*/
    this.apiService.getBankDetails().subscribe(
      res => {
        this.banklist = res.data.bank_details;
        this.bankMandate = res.data.mandate_all;
        this.banklist.forEach(row => {
          this.rowsControls.push({
            isCollapsed: true
          })
        });
        let key = "bank_account"
        localStorage.setItem(key, JSON.stringify(this.banklist));
        localStorage.setItem("allBankMandate", JSON.stringify(this.bankMandate));
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    );
    /*get bank details*/
  }

  showOrHideSearchBox() {
    this.homeElementRef.nativeElement.focus();
    this.showSearchBox = !this.showSearchBox;
  }

  cancelBtn(event: Event) {
    this.selectedValue = '';
    this.loading = false;
    this.showSearchBox = !this.showSearchBox;
    event.stopPropagation();
    this.noResult = false;
  }

  typeaheadNoResults(event: boolean): void {
    this.noResult = event;
  }
  // resultFormatBandListValue(value: any) {            
  //   return value.scheme_name;
  // } 

  // inputFormatBandListValue(value: any)   {
  //   if(value.scheme_name)
  //     return value.scheme_name;
  // }
  // searchBank = (text$: Observable<string>) => {
  //   const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
  //   return merge(debouncedText$).pipe(
  //     map(term => (term === '' ? this.fundsname
  //       : this.fundsname.filter(v => v.scheme_name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
  //       );
  // }
  /*Show Bank List Function*/
  bankChanged() {
    this.showSearchBox = false;
  }

  /*search fund data*/

  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    setTimeout(() => {
      let key = 'scheme_code';
      var object = {};
      this.loading = true;
      object['acc_fin_scheme_code'] = this.selectedOption.acc_fin_scheme_code;
      object['scheme_code'] = this.selectedOption.scheme_code;
      object['isin'] = this.selectedOption.isin;
      this.apiService.getFundDetails(object).subscribe(
        res => {
          this.loading = false;
          let key = 'scheme_details';
          localStorage.setItem(key, JSON.stringify(res.data));
          let isinkey = 'scheme_isin';
          localStorage.setItem(isinkey, JSON.stringify(this.selectedOption.isin));
          this.router.navigate(['/fund-details']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
        }
      )

    }, 200);
  }

  /*search fund data*/

  // Function to get Funds by risk type
  getFundFilterDetails() {
    const personalDetailsFormdata = JSON.stringify(this.riskForm.value);
    const pDetails = JSON.parse(personalDetailsFormdata);
    var object = {};
    object['risk_level'] = pDetails.risk;

    this.loading = true;
    this.apiService.getRiskSchemes(object).subscribe(
      res => {
        let key = 'riskFundDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        // to get risk type on fund-filter page
        let riskkey = 'risk';
        localStorage.setItem(riskkey, pDetails.risk);

        // Remove asset_type & category from local storage
        let asset_type = 'asset_type';
        localStorage.removeItem(asset_type);
        let category = 'category';
        localStorage.removeItem(category);

        // Remove investment plan key from local storage
        let invest_plan = 'investment_plan';
        localStorage.removeItem(invest_plan);

        this.router.navigateByUrl('/fund-filter');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  // Function to get Funds by Cateory
  getFundsByCategory(): void {
    const personalDetailsFormdata = JSON.stringify(this.schemeByCategory.value);
    const pDetails = JSON.parse(personalDetailsFormdata);
    var object = {};

    if (this.viewMode == 'tab1') {
      object['asset_type'] = "Equity";
    }
    else if (this.viewMode == 'tab2') {
      object['asset_type'] = "Debt";
    }
    else {
      object['asset_type'] = "Hybrid";
    }
    object['category'] = pDetails.fundByCategory;

    this.loading = true;
    this.apiService.getFundByCategory(object).subscribe(
      res => {
        let key = 'riskFundDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        let asset_type = 'asset_type';
        localStorage.setItem(asset_type, object['asset_type']);
        let category = 'category';
        localStorage.setItem(category, pDetails.fundByCategory);

        // Remove risk key from local storage 
        let riskkey = 'risk';
        localStorage.removeItem(riskkey);

        // Remove investment plan key from local storage
        let invest_plan = 'investment_plan';
        localStorage.removeItem(invest_plan);

        this.router.navigateByUrl('/fund-filter');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  // Function on click of Top Performing Funds
  investTopFund(funds): void {
    this.loading = true;
    let key = 'scheme_code';
    var object = {};
    object['isin'] = funds.isin;
    object['acc_fin_scheme_code'] = funds.accord_scheme_code;
    object['scheme_code'] = funds.bse_scheme_code;

    this.apiService.getFundDetails(object).subscribe(
      res => {
        this.loading = false;
        let key = 'scheme_details';
        localStorage.setItem(key, JSON.stringify(res.data));
        let isinkey = 'scheme_isin';
        localStorage.setItem(isinkey, JSON.stringify(funds.isin));
        this.router.navigate(['/fund-details']);
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )
  }

  // Function on click of Investment Plans section
  getSchemesByInvestment(): void {
    const personalDetailsFormdata = JSON.stringify(this.schemeByInvestment.value);
    const pDetails = JSON.parse(personalDetailsFormdata);
    var object = {};
    object['type'] = pDetails.fundByInvestment;

    this.loading = true;
    this.apiService.getSchemesByInvestment(object).subscribe(
      res => {
        let key = 'riskFundDetails';
        localStorage.setItem(key, JSON.stringify(res.data));
        // to get investment_plan type on fund-filter page
        let invest_plan = 'investment_plan';
        localStorage.setItem(invest_plan, pDetails.fundByInvestment);

        // Remove asset_type & category from local storage
        let asset_type = 'asset_type';
        localStorage.removeItem(asset_type);
        let category = 'category';
        localStorage.removeItem(category);

        // Remove risk key from local storage 
        let riskkey = 'risk';
        localStorage.removeItem(riskkey);

        this.router.navigateByUrl('/fund-filter');
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.errorHaldlingService.setGlobalValidators(error);
      }
    )

  }

  // FAQ Section
  questions = [
    {
      question: "Can I change the SIP amount?",
      answer: "The amount of existing SIP cannot be changed. The only option is to close the existing SIP and start new SIP with the changed amount in the same scheme.",
    },
    {
      question: "Can I pause my SIP?",
      answer: "Currently Trade Smart - MF does not have the feature to pause the SIP. You will have to cancel the SIP and start a new SIP when you have enough funds to start SIP.",
    },
    {
      question: "If I sold my SIP Mutual Fund partially, in which order will the unit be deducted from my account?",
      answer: "In case of partial redemption the principle of First In First Out (FIFO) is used, it means the oldest units are considered to be redeemed first.",
    },
    {
      question: "I already have a Trading A/c with TradeSmart, do I need to complete the entire registration process?",
      answer: "Yes, you will have to complete the entire registration process even if you have a trading A/C with TradeSmart.",
    },
    {
      question: "Which payment methods are supported on Trade Smart - MF?",
      answer: "The payment could be done through online banking. Trade Smart - MF supports list of 58 banks for online banking.",
    },
    {
      question: "How secure is my investment with TradeSmartMF?",
      answer: "Investment in Mutual funds through Trade Smart - MF is completely safe. All your information is stored using 256-bit encryption.\n Further, all your transactions are done via BSE. At no point does your money reach us either at the time of buying or redeeming the units.",
    },
    {
      question: "What are the charges for investing through Trade Smart - MF?",
      answer: "There are no subscription charges or any hidden charges for investing in Mutual funds through Trade Smart - MF.",
    },
    {
      question: "I have submitted all details required for KYC, but it is still pending verification. When can I expect an update?",
      answer: "Through Trade Smart - MF you can open paperless and instant account for immediate investment in Mutual Fund. However, in case your account opening is pending due to any reason we will get in touch with you at the earliest to get the issue resolved. Post the resolution, account will be opened in 24 working hours. You can also get in touch with us 022-61208000 or email us at support@vnsfin.com.",
    },
    {
      question: "How do I start a SIP on TradeSmart MF App?",
      answer: "Step 1: Select the Mutual fund you want to invest. Use the search feature on the Home screen to select the fund.\n Step 2: Tap 'Monthly SIP' or 'ONE TIME'. \n Step 3: Enter the amount you want to invest. \n Step 4: Confirm and make Payment.",
    },
    {
      question: "Steps for Auto - mandate for SIP",
      answer: "Step 1: Go to Bank Accounts / OTM on Dashboard. \n Step 2: Select Mandates. \n Step 3: Select Bank for SIP. \nMaximum auto-debit limit (default screen) \n Step 4: Next You will receive Mandate form on registered email id. \n Step 5: Sign and Upload the form. \n Step 6: Submit.",
    },
    {
      question: "Can I change the date of my SIP?",
      answer: "You cannot modify an existing SIP. However, you can cancel your existing one and start a new SIP with the required date.",
    },
    {
      question: "Why are some funds showing “SIP not supported”?",
      answer: "In those funds, there is no option to invest through SIP.",
    },
    {
      question: "What does the SIP day/date mean?",
      answer: "It is the day or the date of every month on which your SIP amount will be auto debited from a given bank account for investment in the chosen MF scheme.",
    },
    {
      question: "What happens if you miss a SIP installment?",
      answer: "Your SIP will not become inactive if you happen to miss out on one or two SIP installments. The auto-debit mandate will continue and the SIP amount will be debited on the next SIP date. You will also not be penalised by the fund house for missing the SIP but your bank may penalize you for not maintaining sufficient funds for auto-debit mandate through ECS.",
    },
    {
      question: "When SIP be debited from my account?",
      answer: "The SIP will be debited on the date of every month set by you for auto-debit from your bank account. As SIP transactions are processed by the Bombay Stock Exchange, if on the auto-debit date BSE is closed due to holiday, SIP will be deducted the next working day of BSE.",
    },
    {
      question: "How do I pay for a missed SIP installment?",
      answer: "For any reason, if you happen to miss the SIP date buy lump sum in the same fund for the same amount equal to SIP in the same folio anytime after the SIP date.",
    },
    {
      question: "How do I choose a tenure for my SIP?",
      answer: "The tenure of your SIP will depend on your financial goal. Most fund houses have a minimum SIP tenure of 6 months and can continue SIP till Death. It's better to invest long term to exploit critical benefits of a SIP.",
    },
  ]

  // Risk Section
  risks = [
    {
      type: "LOW RISK", image: "../assets/images/Lowrisk.svg", value: "L",
    },
    {
      type: "MID RISK", image: "../assets/images/Midrisk.svg", value: "M",
    },
    {
      type: "HIGH RISK", image: "../assets/images/Highrisk.svg", value: "H",
    },
  ]

/*Type Head Match Value Trim*/
fixedHighligth(match: TypeaheadMatch, query: Array<String>): String {
  // Avoid calling
  this.query = query.filter((value) => value.trim().length > 0);
  match = new TypeaheadMatch(match.item, match.value, match.isHeader());
  return this.typeahead._container.hightlight(match, this.query);
}
/*Type Head Match Value Trim*/

}
