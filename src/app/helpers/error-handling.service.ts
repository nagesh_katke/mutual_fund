import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class ErrorHaldlingService {
    public errorStatus;
    loading: boolean;
    errorMessage: any;
    loginForm: any;
    otherError: boolean;
    constructor(private router: Router) { }

    /*set field errors*/
    setFieldValidators(res, group: FormGroup): any {
        const statusCode = res.status;
        this.errorStatus = res.status;

        switch (statusCode) {
            case 400:
                const validationErrors = res.error.data;
                Object.keys(validationErrors).forEach(prop => {
                    const formControl = group.get(prop);
                    if (formControl) {
                        // activate the error message
                        formControl.setErrors({
                            serverError: validationErrors[prop]
                        });
                    }
                });
                break;
            case 500:
                this.router.navigate(['/500-page']);
                break;
            case 404:
                this.router.navigate(['/404-page']);
                break;
            case 403:
                localStorage.clear();
                this.router.navigate(['/login']);
                break;
                
        }
    }

    /*set field errors*/

    /*set errors when no form is present*/
    setGlobalValidators(res): any {
        const statusCode = res.status;
        // resolve promise with username
        switch (statusCode) {
            case 500:
                this.router.navigate(['/500-page']);
                break;
            case 404:
                this.router.navigate(['/404-page']);
                break;
            case 403:
                localStorage.clear();
                this.router.navigate(['/login']);
                break;
        }
    }
    /*set errors when no form is present*/
}
