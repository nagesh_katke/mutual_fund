import { FormGroup,  AbstractControl, Validator, ValidationErrors, ValidatorFn } from '@angular/forms';

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }



        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}


export function RelationSelect(control: AbstractControl): { [key: string]: any } | null {
  const relation: string = control.value;
  if (relation === 'Select Your Relation') {
    return { 'RelationSelect': true };
  } else {
        return null;
  }
}



export function OccupationCode(control: AbstractControl): { [key: string]: any } | null {
  const relation: string = control.value;
  if (relation === 'Occupation') {
    return { 'OccupationSelect': true };
  } else {
        return null;
  }
}

export function OccupationType(control: AbstractControl): { [key: string]: any } | null {
  const relation: string = control.value;
  if (relation === 'Occupation Type') {
    return { 'OccupationTypeSelect': true };
  } else {
        return null;
  }
}

export function IncomeSlab(control: AbstractControl): { [key: string]: any } | null {
  const relation: string = control.value;
  if (relation === 'Income Slab') {
    return { 'IncomeSlabSelect': true };
  } else {
        return null;
  }
}


export function sourceSelect(control: AbstractControl): { [key: string]: any } | null {
  const relation: string = control.value;
  if (relation === 'Source of Wealth') {
    return { 'SourceSelect': true };
  } else {
        return null;
  }
}


export function IFSCbankSelect(control: AbstractControl): { [key: string]: any } | null {
  const ifscbank: string = control.value;
  if (ifscbank == '') {
    return { 'IFSCbankSelect': true };
  } else {
        return null;
  }
}


export class CustomValidators {
  static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }

      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);

      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }
}

export class NoWhitespaceValidator {
  static cannotContainSpace(control: AbstractControl) : ValidationErrors | null {
      if((control.value as string).indexOf(' ') >= 0){
          return {cannotContainSpace: true}
      }

      return null;
  }
}
