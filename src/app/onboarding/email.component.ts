import { Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../helpers/api-service.service';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})

@Injectable()

export class EmailComponent implements OnInit {
  emailForm: FormGroup;
  submitted = false;
  loading = false;
  errorMessage = '';
  errorStatus = '';
  otherError = false;
  fundsname: any;
  loginflag: boolean;
  
  @ViewChild('emailRef') emailElementRef: ElementRef;
  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: Location) { }

  ngAfterViewInit(){
      this.emailElementRef.nativeElement.focus();
  }

  ngOnInit() {
    localStorage.clear();
    this.emailForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });

    this.loginflag = false;
      let key = 'loginflag';
      localStorage.setItem(key, JSON.stringify(this.loginflag));
  }

  // convenience getter for easy access to form fields
  get f() { return this.emailForm.controls; }

  saveEmail(): void {
    this.submitted = true;
    if (this.emailForm.valid) {
          this.loading = true;
        const emailFormdata = JSON.stringify(this.emailForm.value);
        const userEmail = JSON.parse(emailFormdata);
        let key = 'email';
        setTimeout(() => {
        this.apiService.emailCheck(userEmail).subscribe(
              res  => {
                this.loading = false;
                localStorage.setItem(key, JSON.stringify(userEmail));
                this.router.navigate(['/set-password']);
                  },
              (error) => {
                this.loading = false;
                this.errorHaldlingService.setFieldValidators(error, this.emailForm);
                this.errorStatus = error.status;
                if(this.errorStatus != '400'){
                  this.otherError = true;
                  this.errorMessage = error.error.data;
                  setTimeout(function() {
                    this.otherError = false;
                }.bind(this), 3000);
                }
                  }
          )
        }, 1000);

    }

  }

}
