import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {WebcamImage, WebcamInitError, WebcamUtil} from 'ngx-webcam';
import {HttpClient} from '@angular/common/http';
import { ImageService } from '../helpers/image.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  public clickPhotoSec = false;
  public showPhotoSec = true;
  public showPhotoCam = false;
  public showGallary = false;
  public webcamImage: WebcamImage = null;
  public pictureTaken = new EventEmitter<WebcamImage>();
  public showWebcam = true;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public errors: WebcamInitError[] = [];

constructor(private http: HttpClient, private imageService: ImageService, public location: Location) { }


/*active camera*/
  dataSource = [];
  onAddData() {
    this.showPhotoSec = false;
    this.showPhotoCam = true;
    this.showGallary = false;
 }

 // webcam snapshot trigger
 private trigger: Subject<void> = new Subject<void>();
 // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
 private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();

 public ngOnInit(): void {
   WebcamUtil.getAvailableVideoInputs()
     .then((mediaDevices: MediaDeviceInfo[]) => {
       this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
     });
 }

 public triggerSnapshot(): void {
   this.trigger.next();
   this.clickPhotoSec = true;
   this.showPhotoCam = false;
 }

 public toggleWebcam(): void {
   this.showWebcam = !this.showWebcam;
 }

 public cancelProcess(): void {
   this.showPhotoSec = true;
   this.showPhotoCam = false;
   this.webcamImage = null;
   this.clickPhotoSec = false;
    this.showGallary = false;
 }

 public handleInitError(error: WebcamInitError): void {
   this.errors.push(error);
 }

 public showNextWebcam(directionOrDeviceId: boolean|string): void {
   this.nextWebcam.next(directionOrDeviceId);
 }

 public handleImage(webcamImage: WebcamImage): void {
   this.pictureTaken.emit(webcamImage);
   this.webcamImage = webcamImage;
 }

 public cameraWasSwitched(deviceId: string): void {
   this.deviceId = deviceId;
 }

 public get triggerObservable(): Observable<void> {
   return this.trigger.asObservable();
 }

 public get nextWebcamObservable(): Observable<boolean|string> {
   return this.nextWebcam.asObservable();
 }

fileToUpload: File = null;
imageUrl: string = "";
/*file upload*/
processFile(file: FileList){
  this.fileToUpload = file.item(0);
  //show image preview
  var reader = new FileReader();
  reader.onload = (event:any) => {
  this.imageUrl = event.target.result;
  }
  reader.readAsDataURL(this.fileToUpload);
  this.showPhotoSec = false;
  this.showGallary = true;
}

onUpload(image){
 this.imageService.uploadImage(this.fileToUpload).subscribe(
   data => {
     image.value = null;
   }
 );
}




/*file upload*/




}
