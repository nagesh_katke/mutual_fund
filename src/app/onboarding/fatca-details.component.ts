import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { IncomeSlab, sourceSelect, OccupationCode, OccupationType } from '../helpers/must-match.validator';
import { ApiServiceService } from '../helpers/api-service.service';
import {Router} from '@angular/router';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-fatca-details',
  templateUrl: './fatca-details.component.html',
  styleUrls: ['./fatca-details.component.css']
})
export class FatcaDetailsComponent implements OnInit {
  fatcadetailsForm: FormGroup;
  loading = false;
  otherError = false;
  errorMessage = '';
  errorStatus = '';
  sourceofwelath: any;
  incomeslab: any;
  occupationCode: any;
  OccupationType: any;
  submitted: boolean;
  status: string;
  authToken: any;

  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService,  public location: Location) { }

  ngOnInit() {
           /*check auth token and go to the home page*/
          let auth_key = 'authentication_token';
           this.authToken = localStorage.getItem(auth_key)
           let account_status = 'account_status';
           this.status = localStorage.getItem(account_status);
           if(this.authToken !== null && this.status >= '40'){
             this.loading = true;
             this.router.navigate(['/home']);
           }
           /*check auth token and go to the home page*/
   
//Get income slab list.
this.incomeslab =[
  {"id" :1, "income_code": "31", "income":"Below 1 Lakh"},
  {"id" :2, "income_code": "32", "income":"Between 1 to 5 Lacs"},
  {"id" :3, "income_code": "33", "income": "Between 5  to 10 Lacs"},
  {"id" :4, "income_code": "34", "income": "Between 10 to 25 Lacs"},
  {"id" :5, "income_code": "35", "income": "Between  25 Lacs to 1 Crore"},
  {"id" :6, "income_code": "36", "income": "Above 1 Crore"},
];

//Get source of welath
this.sourceofwelath =[
  {"id" :1, "source": "Salary", "source_code":"01"},
  {"id" :2, "source": "Business-Income", "source_code":"02"},
  {"id" :3, "source": "Gift", "source_code": "03"},
  {"id" :4, "source": "Ancestral-Property", "source_code": "04"},
  {"id" :5, "source": "Rental Income", "source_code": "05"},
  {"id" :6, "source": " Prize Money", "source_code": "06"},
  {"id" :7, "source": "Royalty", "source_code": "07"},
  {"id" :8, "source": "Others", "source_code": "08"},
];

//Get Occupation Type
this.OccupationType  =[
  {"id" :1, "source": "Service", "source_code":"S"},
  {"id" :2, "source": "Business", "source_code":"B"},
  {"id" :3, "source": "Others", "source_code": "O"},
  {"id" :4, "source": "Not Categorized", "source_code": "X"},
];

//Get occupation
this.occupationCode =[
  {"id" :1, "Occupation": "Business", "Occupation_Code":"01", "Type":"Business"},
  {"id" :2, "Occupation": "Service", "Occupation_Code":"02", "Type":"Service"},
  {"id" :3, "Occupation": "Professional", "Occupation_Code":"03", "Type":"Service"},
  {"id" :4, "Occupation": "Agriculturist", "Occupation_Code":"04", "Type":"Service"},
  {"id" :5, "Occupation": "Retired", "Occupation_Code":"05", "Type":"Others"},
  {"id" :6, "Occupation": "Housewife", "Occupation_Code":"06", "Type":"Others"},
  {"id" :7, "Occupation": "Student", "Occupation_Code":"07", "Type":"Others"},
  {"id" :8, "Occupation": "Others", "Occupation_Code":"08", "Type":"Others"},
  {"id" :9, "Occupation": "Doctor", "Occupation_Code":"09", "Type":"Service"},
  {"id" :10, "Occupation": "Private Sector Service", "Occupation_Code":"41", "Type":"Service"},
  {"id" :11, "Occupation": "Public Sector Service", "Occupation_Code":"42", "Type":"Service"},
  {"id" :12, "Occupation": "Forex Dealer", "Occupation_Code":"43", "Type":"Business"},
  {"id" :13, "Occupation": "Government Service", "Occupation_Code":"44", "Type":"Service"},
  {"id" :14, "Occupation": "Unknown / Not -Applicable", "Occupation_Code":"99", "Type":"Others"},
];


    this.fatcadetailsForm = this.fb.group({
      placeofbirth: ['',  [Validators.required, Validators.pattern(/^[a-zA-Z][a-zA-Z ]+[a-zA-Z]*$/)]],
      source_welath: [{value: "Source of Wealth", disabled: false}, [sourceSelect]],
      incomeSlab: [{value: "Income Slab", disabled: false}, [IncomeSlab]],
      occ_code: [{value: "Occupation", disabled: false}, [OccupationCode]]
    }

  );
  }

 
// convenience getter for easy access to form fields
get f() { return this.fatcadetailsForm.controls; }

/*personal details function*/
saveFatcadetails(): void {
  this.submitted = true;
  if (this.fatcadetailsForm.valid) {
    this.loading = true;
  const fatcaDetailsFormdata = JSON.stringify(this.fatcadetailsForm.value);
  const fDetails = JSON.parse(fatcaDetailsFormdata);
  console.log(fDetails);
  
   var object = {};
  object ['PO_BIR_INC'] = fDetails.placeofbirth;
  object ['SRCE_WEALT'] = fDetails.source_welath;
  object ['INC_SLAB'] = fDetails.incomeSlab;
  object ['OCC_CODE'] = fDetails.occ_code;



  this.apiService.fatcaDetails(object).subscribe(
        res  => {
          this.router.navigate(['/ifsc-code']);
          let account_status = 'account_status';
          localStorage.setItem(account_status, '40');
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.fatcadetailsForm);
          this.errorStatus = error.status;
          if(this.errorStatus != '400'){
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function() {
              this.otherError = false;
          }.bind(this), 3000);
          }
            }
  
    )


}
};




}
