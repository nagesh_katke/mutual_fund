import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';
import { RouterExtService } from '../helpers/previous-router';

@Component({
  selector: 'app-signaturepad-verification',
  templateUrl: './signaturepad-verification.component.html',
  styleUrls: ['./signaturepad-verification.component.css']
})
export class SignaturepadVerificationComponent implements OnInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  @HostListener('window:resize', ['$event'])

  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': window.innerWidth - 90,
    'canvasHeight': window.innerHeight - 50,
  };
  object: any;
  loading: boolean;
  errorStatus: any;
  errorMessage: any;
  fund_status: string;
  authToken: string;
  status: string;
  otherError = false;
  fund_key = 'fund_status';


  constructor(private http: HttpClient, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService ) { 
    
  }

  ngOnInit() {
    this.fund_status = localStorage.getItem(this.fund_key);

    /*check auth token and go to the fund details*/
    let auth_key = 'authentication_token';
    this.authToken = localStorage.getItem(auth_key)
    let account_status = 'account_status';
    this.status = localStorage.getItem(account_status);
    if (this.authToken !== null && this.status == '60') {
      this.loading = true;
      this.router.navigate(['/home']);
    }
    /*check auth token and go to the fund details*/
  }

  ngAfterViewInit() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 1);
    // this.signaturePad is now available
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }

  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
  }

  drawComplete() {
    // will be notified of szimek/signature_pad's onEndEvent
  }

  drawClear() {
    this.signaturePad.clear();
  }

  signSubmit() {
    // to check if Signaturepad is empty
    if (this.signaturePad.isEmpty()) {
      return false;
    }

    this.loading = true;
    var object = {};
    object['signature'] = this.signaturePad.toDataURL();
    this.apiService.signStore(object).subscribe(
      res => {
        this.loading = false;
        this.apiService.getUserdetails().subscribe(
          res => {
            let account_status = 'account_status';
            localStorage.setItem(account_status, res.data.account_status);
          });
        if (this.fund_status === 'true') {
          this.router.navigate(['/fund-details']);
          localStorage.removeItem(this.fund_key);
        }
        else {
          this.router.navigate(['/congratulations']);
        }
      },
      (error) => {
        this.loading = false;
          this.errorHaldlingService.setGlobalValidators(error);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        
      }
    )
  }

}
