import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PhonenumberService } from '../onboarding/phonenumber.service';
import { Router } from '@angular/router';
import { Phonenumber } from '../models/mobile-number.model';
import { ApiServiceService } from '../helpers/api-service.service';
import { countDownTimerConfigModel, CountdownTimerService, countDownTimerTexts } from 'ngx-timer';

@Component({
  selector: 'app-otp-screen',
  templateUrl: './otp-screen.component.html',
  styleUrls: ['./otp-screen.component.css']
})
export class OtpScreenComponent implements OnInit {
  otpForm: FormGroup;
  submitted = false;
  inValid = false;
  errormsg = true;
  loading = false;
  resend = true;
  errorMessage = '';
  errorStatus = '';
  mobilenumber = '';
  errorData = '';
  phonenumbers: Phonenumber[];
  mobile;
  authToken: string;
  status: string;
  @ViewChild('otpRef') otpElementRef: ElementRef;

  constructor(private _phonenumberService: PhonenumberService, private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private countdownTimerService: CountdownTimerService) { }

  testConfig: countDownTimerConfigModel;

  ngAfterViewInit() {
    this.otpElementRef.nativeElement.focus();
  }

  ngOnInit() {
    /*check auth token and go to the home page*/
    let auth_key = 'authentication_token';
    this.authToken = localStorage.getItem(auth_key)
    let account_status = 'account_status';
    this.status = localStorage.getItem(account_status);
    if (this.authToken !== null && this.status >= '20') {
      this.loading = true;
      this.router.navigate(['/home']);
    }
    /*check auth token and go to the home page*/


    this.phonenumbers = this._phonenumberService.getPhonenumber();

    let key = 'mobile';
    this.mobilenumber = localStorage.getItem(key);
    this.otpForm = this.fb.group({
      otp: ['', [Validators.required]],
      otp1: ['', [Validators.required]],
      otp2: ['', [Validators.required]],
      otp3: ['', [Validators.required]],
      otp4: ['', [Validators.required]],
      otp5: ['', [Validators.required]]
    });

    this.testConfig = new countDownTimerConfigModel();
    this.testConfig.timerClass = 'test_Timer_class';
    this.testConfig.timerTexts = new countDownTimerTexts();
    this.testConfig.timerTexts.hourText = " :"; //default - hh
    this.testConfig.timerTexts.minuteText = " :";
    this.testConfig.timerTexts.secondsText = " ";

    this.countdownTimerService.onTimerStatusChange.subscribe(status => {
      if (status = "STOP") {
        this.resend = true;
        this.countdownTimerService.resumeTimer();
      }
    });
  }

  get f() { return this.otpForm.controls; }


  stateChange(event, maxlength) {
    let nextInput = event.srcElement.nextElementSibling;
    let perInput = event.srcElement.previousElementSibling; // get the sibling element
    let vInput = event.srcElement.value;

    var key = event.keyCode || event.charCode;
    if (key == 8 || key == 46) {
      perInput.focus();
    }

    else {
      nextInput.focus();   // focus if not null
    }


  }

  startTimerGT() {
    let cdate = new Date();
    cdate.setMinutes(cdate.getMinutes() + 2);
    cdate.setSeconds(cdate.getSeconds() + 0);
    this.countdownTimerService.startTimer(cdate);
  }

  resumeTimer() {
    this.countdownTimerService.resumeTimer();
  }

  resendOtp(): void {
    this.startTimerGT();
    this.inValid = false;
    this.resend = false;
    let emailkey = 'email';
    let item = JSON.parse(localStorage.getItem(emailkey));
    var object = {};
    object['email'] = item.email;
    object['otp_flag'] = 'M';
    object['mobile'] = this.mobilenumber;
    this.apiService.resendotpVerification(object).subscribe(
      res => {
      },
      (error) => {
        this.errorMessage = error.data;
        this.errorStatus = error.status;
        let status: number = parseFloat(this.errorStatus);
        switch (status) {
          case 200: {
            this.errorData = error.error.data;
            break;
          }
          case 417: {
            this.errorData = error.error.data.otp;
            break;
          }
          default: {
            break;
          }
        }
      }
    )
  }


  sendOtp(): void {
    this.submitted = true;
    this.errorData = '';
    if (this.otpForm.valid) {
      this.loading = true;
      this.inValid = false;
      let key = 'email';
      let item = JSON.parse(localStorage.getItem(key));

      const otpNumber = this.otpForm.value;
      const otpN: number = otpNumber.otp + otpNumber.otp1 + otpNumber.otp2 + otpNumber.otp3 + otpNumber.otp4 + otpNumber.otp5;
      var object = {};
      object['otp'] = otpN;
      object['email'] = item.email;
      object['mobile'] = this.mobilenumber;

      this.apiService.otpVerification(object).subscribe(
        res => {
          this.loading = false;
          let account_status = 'account_status';
          localStorage.setItem(account_status, '20');
          this.router.navigate(['/pan-number']);
        },
        (error) => {
          this.loading = false;
          this.errorMessage = error.data;
          this.errorStatus = error.status;
          let status: number = parseFloat(this.errorStatus);
          switch (status) {
            case 403: {
              this.errorData = error.error.data;
              break;
            }
            case 400: {
              this.errorData = error.error.data.otp;
              break;
            }
            default: {
              this.errorData = error.error.data;
              break;
            }
          }
        }
      )
    }
    else {
      this.inValid = true;

    }
  }
}
