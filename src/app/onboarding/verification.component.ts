import {  Component, OnInit, Inject } from '@angular/core';
import {Router} from '@angular/router';
import { DOCUMENT, Location } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private document: Document, private router: Router, public location: Location) { }
  citizenCheckbox: FormControl = new FormControl();
  termCheckbox: FormControl = new FormControl();
  ngOnInit() {
    

  }



  saveVerification(): void {
    this.document.body.classList.add('test');
    this.router.navigate(['/signaturepad-verification']);
  }
}
