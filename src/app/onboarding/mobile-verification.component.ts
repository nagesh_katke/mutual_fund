import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PhonenumberService } from '../onboarding/phonenumber.service';
import { Router } from '@angular/router';
import { Phonenumber } from '../models/mobile-number.model';
import { ApiServiceService } from '../helpers/api-service.service';
import { Location } from '@angular/common';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-mobile-verification',
  templateUrl: './mobile-verification.component.html',
  styleUrls: ['./mobile-verification.component.css']
})
export class MobileVerificationComponent implements OnInit {
  mobileverificationForm: FormGroup;
  submitted = false;
  loading = false;
  errorMessage = '';
  errorStatus = '';
  @ViewChild('mobnoRef') mobnoElementRef: ElementRef;

  phonenumber: Phonenumber = {
    mobile: null
  }
  otherError: boolean;


  constructor(private fb: FormBuilder, private _phonenumberService: PhonenumberService, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService, public location: Location) {
  }

  ngAfterViewInit() {
    this.mobnoElementRef.nativeElement.focus();
  }

  ngOnInit() {
    this.mobileverificationForm = this.fb.group({
     // mobile: ['', [Validators.required, Validators.minLength(10)]]
      mobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]]
    });
  }


  // convenience getter for easy access to form fields
  get f() { return this.mobileverificationForm.controls; }



  savePhonenumber(): void {
    this.submitted = true;
    let key = 'mobile';
    if (this.mobileverificationForm.valid) {
      this.loading = true;
      const mobileNumber = JSON.stringify(this.mobileverificationForm.value);
      const userMobile = JSON.parse(mobileNumber);
      this.apiService.mobileVerification(userMobile).subscribe(
        res => {
          this.loading = false;
          localStorage.setItem(key, userMobile.mobile);
          this.router.navigate(['/otp']);
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.mobileverificationForm);
          this.errorStatus = error.status;
          if(this.errorStatus != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function() {
              this.otherError = false;
          }.bind(this), 3000);
          }
        }
      )


    }
  }




}
