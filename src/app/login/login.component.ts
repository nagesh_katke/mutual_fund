import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../helpers/api-service.service';
import { Router } from '@angular/router';
import { ErrorHaldlingService } from '../helpers/error-handling.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  flag = 'true';
  showBank = true;
  submitted = false;
  loading = false;
  errorMessage = '';
  errorStatus = '';
  otherError = false;
  fundsname: any;
  @ViewChild('passRef') passElementRef: ElementRef;
  @ViewChild('showhideRef') showhideElementRef: ElementRef;
  @ViewChild('emailRef') emailElementRef: ElementRef;
  authToken: any;

  constructor(private fb: FormBuilder, private router: Router, private apiService: ApiServiceService, private errorHaldlingService: ErrorHaldlingService) { }

  ngOnInit() {

    //pushing two states in window.history inside ngOnInit(). Firstly the URL (/profile) I want to visit on the back key press and then the current URL (/show-bank).
    window.history.pushState({}, 'Account', '/login');
    window.history.pushState({}, 'Bank-Mandate details', '');

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });

    /*check auth token and go to the home page*/
    let auth_key = 'authentication_token';
    this.authToken = localStorage.getItem(auth_key);
    let account_status = 'account_status';
    if (this.authToken !== null) {
      this.loading = true;
      this.router.navigate(['/home']);
    }
    /*check auth token and go to the home page*/
  }

  ngAfterViewInit() {
    this.emailElementRef.nativeElement.focus();
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  showOrHideBankNumber() {
    this.showBank = !this.showBank;
    if (this.showBank) {
      this.passElementRef.nativeElement.classList.add("passtxt");
      this.showhideElementRef.nativeElement.classList.remove("fa-eye-slash");
      this.showhideElementRef.nativeElement.classList.add("fa-eye");
    }
    else {
      this.passElementRef.nativeElement.classList.remove("passtxt");
      this.showhideElementRef.nativeElement.classList.add("fa-eye-slash");
      this.showhideElementRef.nativeElement.classList.remove("fa-eye");
    }
  }

  saveLogin(): void {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.loading = true;
      const loginFormdata = JSON.stringify(this.loginForm.value);
      const userLogin = JSON.parse(loginFormdata);
      this.apiService.login(userLogin).subscribe(
        res => {
          let key = 'authentication_token';
          let account_status = 'account_status';
          localStorage.setItem(key, res.data.authentication_token);
          localStorage.setItem(account_status, res.data.account_status);

          /*Start fund list api*/
          this.apiService.getFundName().subscribe(
            res => {
              this.fundsname = res.data;
              let fund_list = 'fund_list';
              // localStorage.clear();
              localStorage.setItem(fund_list, JSON.stringify(this.fundsname));
              this.loading = false;
              this.router.navigate(['/home']);
            },
            (error) => {

            }
          )
          /*End fund list api*/
        },
        (error) => {
          this.loading = false;
          this.errorHaldlingService.setFieldValidators(error, this.loginForm);
          this.errorStatus = error.status;
          if (error.status != "400") {
            this.otherError = true;
            this.errorMessage = error.error.data;
            setTimeout(function () {
              this.otherError = false;
            }.bind(this), 3000);
          }
        }
      )
    }
  }

  /*start Forgot Password*/
  forgotPassword(): void {
    let key = 'forgotFlag';
    localStorage.setItem(key, this.flag);
    this.router.navigate(['/reset-password']);
  }
  /*end Forgot Password*/

}
