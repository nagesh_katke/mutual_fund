import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// if (environment.production) {
//   enableProdMode();
// }

// platformBrowserDynamic().bootstrapModule(AppModule)
//   .catch(err => console.error(err));


const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule).catch(e => console.error(e));
const bootstrapCordova = () => {
  /** Dynamically load cordova JS **/
  console.log('bootstrapped cordova');

  const script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'cordova.js';

  document.head.appendChild(script);

  if (script['readyState']) {
    // IE 2018 and earlier (?). They're migrating to using Chromium under the hood, so this may change
    script['onreadystatechange'] = () => {
      if (script['readyState'] === "loaded" || script['readyState'] === "complete") {
        document.addEventListener('deviceready', bootstrap, false);

      }
    };
  } else {
    // All other browsers
    document.addEventListener('deviceready', bootstrap, false);
  }
};

if (environment.production) {
  enableProdMode();
  bootstrapCordova();
}else{
  bootstrap();
  enableProdMode();
}
